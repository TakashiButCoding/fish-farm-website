#!/usr/bin/env bash

# ensure to stop running container
docker  stop    rai-fish-farm-be-qa
docker  stop    rai-fish-farm-be-prod

docker  rm      rai-fish-farm-be-qa
docker  stop    rai-fish-farm-be-prod

# delete all images that's not tagged and not being used by any container
docker images prune

# pull latest db
# backend
docker  pull    charnkanit/rai-fish-farm-be-qa:latest
docker  run --add-host=host.docker.internal:172.17.0.1 -d --name rai-fish-farm-be-prod -p 4000:4000 charnkanit/rai-fish-farm-be-prod:latest