import React, { useState } from "react";
import "../style/About.css";

function AboutCard({ mainPic, projectName, beforeViewContent, img1, img2, img3, afterViewContent }) {
  const [expanded, setExpanded] = useState(false);

  const handleViewClick = () => {
    setExpanded(true);
  };

  const handleHideClick = () => {
    setExpanded(false);
  };

  return (
    <div className="about-container">
      <div className="before-view">
        <div className="pic-container">
          <img src={mainPic} className="main-image" />
        </div>
        <div className="content-container">
          <div className="second-content-container">
            <p className="bold">{projectName}</p>
            <p>{beforeViewContent}</p>
          </div>
          {!expanded && (
            <div className="button-container">
              <button type="button" onClick={handleViewClick}>
                View
              </button>
            </div>
          )}
        </div>
      </div>
      {expanded && (
        <div className="after-view">
          <div className="img-content">
            <img src={img1} className="content-image" />
            <img src={img2} className="content-image" />
            <img src={img3} className="content-image" />
          </div>
          <div className="additional-content">
            <p>{afterViewContent}</p>
            <div className="button-container">
              <button type="button" onClick={handleHideClick}>
                Hide
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default AboutCard;
