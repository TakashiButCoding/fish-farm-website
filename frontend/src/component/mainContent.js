import React from "react";
import { useMediaQuery } from "react-responsive";
function MainContent({ children }) {
    let isPc = useMediaQuery({ query: "(max-height: 1024px)" });
    let isTabletMid = useMediaQuery({ query: "(max-width: 768px)" });
  return (
    <div className={`relative  px-5 pt-32 pb-3 my-auto w-full z-[1] `}>
        {children}
    </div>
  );
}
//${!isTabletMid || !isPc ? "py-0" : "py-20"}
export default MainContent;
