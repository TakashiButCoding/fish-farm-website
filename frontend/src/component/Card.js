// Card.js
import React from "react";
import "../style/Card.css";

function Card({
  title,
  measurement1,
  unit1,
}) {
  return (
    <div className="card">
      <h3 className="title">{title}</h3>

      <div className="measurement-container">
        <div className="value-header">
          <h1 className="measurement text-m">{measurement1}</h1>
        </div>
        <p className="unit">{unit1}</p>
      </div>

    </div>
  );
}

export default Card;
