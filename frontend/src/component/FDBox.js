import "../style/FDStyle.css";
import { motion } from "framer-motion";

export default function FDBox({children, width}){

    return (
        <motion.div
            whileHover={{ 
                scale: 1.03, 
                boxShadow: "0px 0px 30px -15px rgba(0,0,0,0.62)"
            }}
            whileTap={{ 
                scale: 0.99,
            }}
            className="FDBox"
        >
            {children}
        </motion.div>
    )
}