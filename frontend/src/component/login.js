import { GoogleLogin } from "@react-oauth/google";
function Login() {
  return (
    <div id="signInButton">
      <GoogleLogin
        onSuccess={(credentialResponse) => {
          console.log("login success");
          console.log(credentialResponse);
        }}
        onError={() => {
          console.log("Login Failed");
        }}
      />
    </div>
  );
}

export default Login;
