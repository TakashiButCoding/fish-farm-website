// Weather.js
import React, { useState, useEffect } from "react";
import axios from "axios";
import FDBox from "../component/FDBox";
import {
  MDBCard,
  MDBCardBody,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBRow,
  MDBTypography,
} from "mdb-react-ui-kit";

const Weather = ({ latitude, longitude, time }) => {
  const [weatherData, setWeatherData] = useState(null);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const getWeatherData = async () => {
      try {
        const response = await axios.get(
          `https://api.openweathermap.org/data/2.5/weather?units=metric&lat=${latitude}&lon=${longitude}&appid=965890225e91cabef07d40247d4a5203`
        );
        setWeatherData(response.data);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching weather data:", error);
        setLoading(false);
      }
    };

    getWeatherData();

    const intervalWeather = setInterval(getWeatherData, 300000);
    return () => {
      clearInterval(intervalWeather);
    };
  }, [latitude, longitude]);
  if (loading) {
    return (
      <div className="loading-spinner">
        <div className="spinner"></div>
      </div>
    );
  }
  return (
    <FDBox className="basis-1/4 grow flex justify-center flex-row items-stretch">
      <div className="d-flex w-48">
        <MDBTypography tag="h6" className="flex-grow-1">
          {weatherData.name} {weatherData.sys.country}
        </MDBTypography>
        <MDBTypography tag="h6">{time}</MDBTypography>
      </div>

      <div className="d-flex flex-column text-center mt-5 mb-4 ">
        <MDBTypography
          tag="h6"
          className="display-4 mb-0 font-weight-bold"
          style={{ color: "#1C2331" }}
        >
          {" "}
          {weatherData.main.temp} °C{" "}
        </MDBTypography>
        <span className="small" style={{ color: "#868B94" }}>
          {weatherData.weather[0].description}
        </span>
      </div>

      <div className="d-flex align-items-center">
        <div className="flex-grow-1" style={{ fontSize: "1rem" }}>
          <div className="flex justify-between items-center">
            <span className="ms-1 text-left">Wind:</span>
            <span className="ms-1 text-right">
              {weatherData.wind.speed} km/h
            </span>
          </div>
          <div className="flex justify-between items-center">
            <span className="ms-1 text-left">Humidity:</span>
            <span className="ms-1 text-right">
              {weatherData.main.humidity}%
            </span>
          </div>
        </div>
        <div className="flex justify-center items-center h-full">
          <img
            src={`https://openweathermap.org/img/wn/${weatherData.weather[0].icon}@2x.png`}
            width="100px"
            className="items-center"
          />
        </div>
      </div>
    </FDBox>
  );
};

export default Weather;
