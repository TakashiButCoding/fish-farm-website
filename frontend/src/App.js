import React, { useState } from "react";
import { Routes, Route,  } from "react-router-dom";
import SensorMonitor from "./pages/sensorMonitorPage";
import ImageGallery from "./pages/ImageGallery";
import RootLayout from "./layouts/rootLayout";
import NoPage from "./component/http error/404";
import ImageFrame from "./pages/imageFrame";
import Feeder from "./pages/feeder";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import Webpage from "./pages/webpage";

import "./App.css";

const App = () => {
  return (
    <RootLayout>
        <Routes>
          <Route path="/" element={<Webpage />} />
          <Route path="sensorMonitoring" element={<SensorMonitor />} />
          <Route path="imageGallery/:page" element={<ImageGallery />} />
          <Route path="image/:page/:id" element={<ImageFrame />} />
          <Route path="feeder" element={<Feeder />} />
          <Route path="*" element={<NoPage />} />
        </Routes>
    </RootLayout>
  );
};

export default App;
