import { useEffect, useState } from "react";
import { useRef } from "react";
import SubMenu from "./SubMenu";
import { motion } from "framer-motion";
import logo from "../../component/asset/logo.png"; // Import your logo image
import { VERSION } from "../../version";

// * React icons
import { IoIosArrowBack } from "react-icons/io";
import { SlSettings } from "react-icons/sl";
import { AiOutlineAppstore } from "react-icons/ai";
import { FaRegImages } from "react-icons/fa6";
import { FaFishFins } from "react-icons/fa6";
import { BsPerson } from "react-icons/bs";
import { MdOutlineSensors } from "react-icons/md";
import { IoHome } from "react-icons/io5";
import { HiOutlineDatabase } from "react-icons/hi";
import { TbReportAnalytics } from "react-icons/tb";
import { RiBuilding3Line } from "react-icons/ri";
import { useMediaQuery } from "react-responsive";
import { MdMenu } from "react-icons/md";
import { NavLink, useLocation, useRoutes } from "react-router-dom";

const Sidebar = () => {
  let isTabletMid = useMediaQuery({ query: "(max-width: 768px)" });
  const [open, setOpen] = useState(isTabletMid ? false : true);
  const sidebarRef = useRef();
  const { pathname } = useLocation();

  useEffect(() => {
    if (isTabletMid) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  }, [isTabletMid]);

  useEffect(() => {
    isTabletMid && setOpen(false);
  }, [pathname]);

  const Nav_animation = isTabletMid
    ? {
        open: {
          x: 0,
          width: "18rem",
          transition: {
            damping: 40,
          },
        },
        closed: {
          x: -250,
          width: 0,
          transition: {
            damping: 40,
            delay: 0.15,
          },
        },
      }
    : {
        open: {
          width: "18rem",
          transition: {
            damping: 40,
          },
        },
        closed: {
          width: "4rem",
          transition: {
            damping: 40,
          },
        },
      };

  const subMenusList = [
    {
      name: "build",
      icon: RiBuilding3Line,
      menus: ["auth", "app settings", "stroage", "hosting"],
    },
    {
      name: "analytics",
      icon: TbReportAnalytics,
      menus: ["dashboard", "realtime", "events"],
    },
  ];

  return (
    <div>
      <div
        onClick={() => setOpen(false)}
        className={`md:hidden fixed inset-0 max-h-screen z-[3] bg-black/50 ${
          open ? "block" : "hidden"
        } `}
      ></div>
      <motion.div
        ref={sidebarRef}
        variants={Nav_animation}
        initial={{ x: isTabletMid ? -250 : 0 }}
        animate={open ? "open" : "closed"}
        className="bg-white text-gray shadow-xl z-[4] max-w-[18rem]  w-[18rem]
            overflow-hidden md:relative absolute 
         h-screen max-h-screen"
      >
        <div className="flex items-center gap-2.5 font-medium border-b py-3 border-slate-300  mx-3">
          <img src={logo} alt="KMITL Logo" width={40} />
          <span className="text-xl whitespace-pre">RAI Smart Fish</span>
        </div>

        <div className="flex flex-col  h-full">
          <ul className="whitespace-pre px-2.5 text-[0.9rem] py-5 flex flex-col gap-1  font-medium overflow-x-hidden scrollbar-thin scrollbar-track-white scrollbar-thumb-slate-100   md:h-[68%] h-[70%]">
            <li>
              <NavLink to={"/"} className="link">
                <IoHome size={23} className="min-w-max" />
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to={"/sensorMonitoring"} className="link">
                <MdOutlineSensors size={23} className="min-w-max" />
                Water Quality Measurement
              </NavLink>
            </li>
            <li>
              <NavLink to={"/feeder"} className="link">
                <FaFishFins size={23} className="min-w-max" />
                Feedy
              </NavLink>
            </li>
            <li>
              <NavLink to={"/imageGallery/1"} className="link">
                <FaRegImages size={23} className="min-w-max" />
                Image Gallery
              </NavLink>
            </li>
            {/*
            {(open || isTabletMid) && (
              <div className="border-y py-5 border-slate-300 ">
                <small className="pl-3 text-slate-500 inline-block mb-2">
                  Product categories
                </small>
                {subMenusList?.map((menu) => (
                  <div key={menu.name} className="flex flex-col gap-1">
                    <SubMenu data={menu} />
                  </div>
                ))}
              </div>
            )}
            <li>
              <NavLink to={"/settings"} className="link">
                <SlSettings size={23} className="min-w-max" />
                Settings
              </NavLink>
            </li>
          */}
          </ul>
          {/* 
          {open && (
            <div className="flex-1 text-sm z-50  max-h-48 my-auto  whitespace-pre   w-full  font-medium  ">
              <div className="flex border-y border-slate-300 p-4 items-center justify-between">
                <div>
                  <p>Spark</p>
                  <small>No-cost $0/month</small>
                </div>
                <p className="text-teal-500 py-1.5 px-3 text-xs bg-teal-50 rounded-xl">
                  Upgrade
                </p>
              </div>
            </div>
          )}
          */}
        </div>
        <motion.div
          onClick={() => {
            setOpen(!open);
          }}
          animate={
            open
              ? {
                  x: -10,
                  y: 0,
                  rotate: 0,
                }
              : {
                  x: -10,
                  y: 0,
                  rotate: 180,
                }
          }
          transition={{ duration: 0 }}
          className="absolute w-fit h-fit md:block z-50 hidden right-2 bottom-3 cursor-pointer"
        >
          <IoIosArrowBack size={25} />
        </motion.div>
        <motion.div
          animate={
            open
              ? {}
              : {
                  display: "none",
                }
          }
          className="absolute bottom-0 left-0 p-4 text-gray text-sm"
        >
          Version: {VERSION}
        </motion.div>
      </motion.div>

      <div className="md:hidden w-full h-full bg-white shadow-l z-[4]">
        <div className="flex items-center gap-2.5 font-medium ">
          <MdMenu size={40} onClick={() => setOpen(true)} className="m-3" />
          <img src={logo} alt="KMITL Logo" width={40} />
          <span className="text-xl text-bold whitespace-pre">RAI Smart Fish</span>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
