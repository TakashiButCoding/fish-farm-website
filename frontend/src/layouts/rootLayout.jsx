import Sidebar from "./sidebar";
import { useMediaQuery } from "react-responsive";
import { useLocation } from "react-router-dom";

function RootLayout({ children }) {
  const location = useLocation();
  let isTabletMid = useMediaQuery({ query: "(max-width: 768px)" });
  if (location.pathname !== "/") {
    return (
      <div className={`gap-0 scrollbar-hidden ${isTabletMid ? "" : "flex"} `}>
        <Sidebar />
        <main className="max-w-full flex-1 mx-auto max-h-screen overflow-auto">
          {children}
        </main>
      </div>
    );
  }
  return (
    <div className={`gap-0 scrollbar-hidden ${isTabletMid ? "" : "flex"} `}>
      <main className="max-h-screen overflow-auto">
        {children}
      </main> 
    </div>
  );
}

export default RootLayout;
