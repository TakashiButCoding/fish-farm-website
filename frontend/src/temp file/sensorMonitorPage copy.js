import React, { useState, useEffect } from "react";
import Card from "../component/Card";
import "../style/loading.css";
import API from "../api";
import ReqTimedOut from "../component/http error/408";
import PageHeader from "../component/pageHeader";
import MainContent from "../component/mainContent";
import Weather from "../component/Weather";
// Add a helper function to determine pH level
function getPHLevel(ph) {
  if (ph < 7) {
    return "Acidic";
  } else if (ph === 7) {
    return "Neutral";
  } else {
    return "Alkaline";
  }
}
function SensorMonitor() {
  const [sensorData, setSensorData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [reqTimedOut, setReqTimedOut] = useState(false);
  const [currentTime, setCurrentTime] = useState("");
  const [selectedDateRange, setSelectedDateRange] = useState([
    new Date("2024-01-01"),
    new Date("2024-12-01"),
  ]);
  const handleDateRangeChange = (dateRange) => {
    setSelectedDateRange(dateRange);
    //console.log(selectedDateRange);
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await API.getLatestSensorData();
        setSensorData(response);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching sensor data:", error);
        setLoading(false);
        setReqTimedOut(true);
      }
    };
    const intervalId = setInterval(() => {
      const utcPlus7Time = new Date();
      utcPlus7Time.setHours(utcPlus7Time.getHours() + 7);

      const formattedTime = utcPlus7Time.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        timeZone: "UTC",
      });

      setCurrentTime(formattedTime);
    }, 1000);
    // Fetch data initially
    fetchData();

    // Fetch data every minute
    const intervalSensor = setInterval(fetchData, 10000);

    // Cleanup interval on unmount
    return () => {
      clearInterval(intervalId);
      clearInterval(intervalSensor);
    };
  }, [selectedDateRange]);

  if (loading) {
    return (
      <div className="loading-spinner">
        <div className="spinner"></div>
      </div>
    );
  }
  if (reqTimedOut) {
    return <ReqTimedOut />;
  }
  const groupedSensorData = sensorData.reduce((acc, sensor) => {
    const sensorNodeId = sensor.sensor_node_id;
    if (!acc[sensorNodeId]) {
      acc[sensorNodeId] = [];
    }
    acc[sensorNodeId].push(sensor);
    return acc;
  }, {});

  return (
    <div>
      <PageHeader
        title="Water Quality Measurement"
        onDateRangeChange={handleDateRangeChange}
      />
      <MainContent>
        <div className="relative my-auto w-full">
          {Object.entries(groupedSensorData).map(([sensorNodeId, sensors]) => (
            <div key={sensorNodeId}>
              {/* Wrap the card elements in a container with a fixed width */}
              {sensors.map((sensor, index) => (
                <div key={index}>
                  <div className="flex justify-between items-center">
                    <div className="text-l font-bold text-left">
                      Sensor {sensorNodeId}
                    </div>
                    <div className="text-sm text-right px-2">
                      ({sensor.recorded_when})
                    </div>
                  </div>
                  <div className="flex justify-between">
                    {
                      <Card
                        title={`Dissolved Oxygen`}
                        measurement1={`${sensor.dissolved_oxygen}`}
                        unit1={`%`}
                      />
                    }
                    {
                      <Card
                        title={`Electric Conductivity`}
                        measurement1={`${sensor.electric_conductivity}`}
                        unit1={`ms/cm`}
                        className="grow"
                      />
                    }
                    {
                      <Card
                        title={`Temperature`}
                        measurement1={`${sensor.temperature}`}
                        unit1={`ํC`}
                        className="grow"
                      />
                    }
                    {
                      <Card
                        title={`pH`}
                        measurement1={`${sensor.pH}`}
                        unit1={`(${getPHLevel(sensor.pH)})`}
                      />
                    }
                  </div>
                  <br />
                </div>
              ))}
            </div>
          ))}
        </div>
        <Weather
          latitude={13.726596724980457}
          longitude={100.77613121944981}
          time={currentTime}
        />
      </MainContent>
    </div>
  );
}
export default SensorMonitor;
