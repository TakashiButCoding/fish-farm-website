import React, { useState, useEffect } from "react";
import API from "../api";
import "../style/loading.css";
import ReqTimedOut from "../component/http error/408";
import PageHeader from "../component/pageHeader";
import MainContent from "../component/mainContent";
import { Buffer } from "buffer";
import { Link, useParams } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";

function ImageGallery() {
  const { page } = useParams();
  const [images, setImages] = useState([]);
  const [loading, setLoading] = useState(true);
  const [reqTimedOut, setReqTimedOut] = useState(false);
  const [selectedDateRange, setSelectedDateRange] = useState([
    new Date("2024-01-01"),
    new Date("2024-12-01"),
  ]);
  const [_page, setPage] = useState(page);
  let isTabletMid = useMediaQuery({ query: "(max-width: 768px)" });
  let slice = Math.floor(window.innerWidth / 350);

  const handleDateRangeChange = (dateRange) => {
    setSelectedDateRange(dateRange);
    console.log(selectedDateRange)
  };

  const handlePageChange = () => {
    setPage(page)
  };

  const fetchImages = async () => {
    try {
      const payload = {
        pagination_no: page,
        fetch_limit: 8,
        date_start: selectedDateRange[0],
        date_end: selectedDateRange[1],
      };
      const response = await API.getImages(payload);
      if (Array.isArray(response)) {
        setImages(response);
      }
      setLoading(false);
    } catch (error) {
      console.error("Error fetching images:", error);
      setLoading(false);
    }
  };

  useEffect(() => {
    setLoading(true);
    fetchImages();
  }, [selectedDateRange, _page]);

  if (loading) {
    return (
      <div className="loading-spinner">
        <div className="spinner"></div>
      </div>
    );
  }
  if (reqTimedOut) {
    return <ReqTimedOut />;
  }
  // Split images into rows with 3 pictures each
  const rows = [];
  for (let i = 0; i < images.length; i += slice) {
    const rowImages = images.slice(i, i + slice);
    rows.push(rowImages);
  }

  return (
    <div>
      <PageHeader
        title="Image Gallery"
        onDateRangeChange={handleDateRangeChange}
      />
      <MainContent>
        <div className="">
          {rows.map((row, index) => (
            <div key={index} className="flex justify-center py-1">
              {row.map((image) => (
                <Link
                  key={image.id}
                  to={`/image/${page}/${image.id}`} // Use a route for individual images
                  className="m-2 cursor-pointer"
                >
                  <img
                    src={`data:image/png;base64,${Buffer.from(
                      image.data.data
                    ).toString("base64")}`}
                    alt={image.filename}
                    className="w-64 h-64 object-cover"
                  />
                  <div className="text-center mt-1 bg-gray">
                    {new Date(image.recorded_when).toLocaleString()}
                  </div>
                </Link>
              ))}
            </div>
          ))}
        </div>
        <Pagination
          className="py-5 flex justify-center"
          page={page}
          count={10}
          onChange={handlePageChange}
          renderItem={(item) => (
            <PaginationItem
              component={Link}
              to={`/imageGallery/${item.page}`}
              {...item}
            />
          )}
        />
      </MainContent>
    </div>
  );
}

export default ImageGallery;
