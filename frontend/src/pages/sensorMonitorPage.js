import React, { useState, useEffect } from "react";
//import Card from "../component/Card";
import "../style/loading.css";
import API from "../api";
import ReqTimedOut from "../component/http error/408";
import PageHeader from "../component/pageHeader";
import MainContent from "../component/mainContent";
import Weather from "../component/Weather";
import { Gauge, gaugeClasses } from "@mui/x-charts/Gauge";
import FDBox from "../component/FDBox";
import { useMediaQuery } from "react-responsive";
import Chart from "../component/LineChart";

// Add a helper function to determine pH level
function getPHLevel(ph) {
  if (ph < 7) {
    return "Acidic";
  } else if (ph === 7) {
    return "Neutral";
  } else {
    return "Alkaline";
  }
}
function SensorMonitor() {
  let isTabletMid = useMediaQuery({ query: "(max-width: 768px)" });
  const [sensorData, setSensorData] = useState([]);
  const [sensorDataTimeSeries, setSensorDataTimeSeries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [reqTimedOut, setReqTimedOut] = useState(false);
  const [currentTime, setCurrentTime] = useState("");

  const today = new Date();
  const yesterday = new Date(today);
  yesterday.setDate(today.getDate() - 1);
  const [selectedDateRange, setSelectedDateRange] = useState([
    yesterday,
    new Date(),
  ]);
  const handleDateRangeChange = (dateRange) => {
    setSelectedDateRange(dateRange);
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await API.getLatestSensorData();
        setSensorData(response);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching sensor data:", error);
        setLoading(false);
        setReqTimedOut(true);
      }
    };
    const fetchDataByTime = async () => {
      try {
        const payload = {
          timerange_begin: selectedDateRange[0],
          timerange_end: selectedDateRange[1],
        };
        const response = await API.getSensorData(payload);
        setSensorDataTimeSeries(response);
        setLoading(false);
        //console.log(sensorDataTimeSeries)
      } catch (error) {
        console.error("Error fetching sensor data:", error);
        setLoading(false);
        setReqTimedOut(true);
      }
    };
    const intervalId = setInterval(() => {
      const utcPlus7Time = new Date();
      utcPlus7Time.setHours(utcPlus7Time.getHours() + 7);

      const formattedTime = utcPlus7Time.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        timeZone: "UTC",
      });

      setCurrentTime(formattedTime);
    }, 1000);
    // Fetch data initially
    fetchData();
    fetchDataByTime();
    // Fetch data every minute
    const intervalSensor = setInterval(fetchData, 60000);

    // Cleanup interval on unmount
    return () => {
      clearInterval(intervalId);
      clearInterval(intervalSensor);
    };
  }, [selectedDateRange]);

  if (loading) {
    return (
      <div className="loading-spinner">
        <div className="spinner"></div>
      </div>
    );
  }
  if (reqTimedOut) {
    return <ReqTimedOut />;
  }
  const groupedSensorData = sensorData.reduce((acc, sensor) => {
    const sensorNodeId = sensor.sensor_node_id;
    if (!acc[sensorNodeId]) {
      acc[sensorNodeId] = [];
    }
    acc[sensorNodeId].push(sensor);
    return acc;
  }, {});
  return (
    <div>
      <PageHeader
        title="Water Quality Measurement"
        onDateRangeChange={handleDateRangeChange}
      />
      <MainContent>
        <div className="relative max-h-fit my-auto w-full">
          {Object.entries(groupedSensorData).map(([sensorNodeId, sensors]) => (
            <div key={sensorNodeId}>
              {/* Wrap the card elements in a container with a fixed width */}
              {sensors.map((sensor, index) => (
                <div key={index}>
                  <div className="flex justify-between items-center">
                    <div className="text-l font-bold text-left">
                      Sensor {sensorNodeId}
                    </div>
                    <div className="text-sm text-right px-2">
                      ({sensor.recorded_when})
                    </div>
                  </div>
                  <div
                    className={`${isTabletMid ? "" : "flex justify-between"} `}
                  >
                    {/* {
                      <FDBox className="basis-1/4 grow flex flex-row items-center items-stretch">
                        <div className="h-full">
                          <div className="flex justify-center">
                            <h1>Dissolve Oxygen</h1>
                          </div>
                          <div className="h-5/6 w-48 items-center flex justify-center">
                            <Gauge
                              value={sensor.dissolved_oxygen}
                              innerRadius="80%"
                              outerRadius="100%"
                              sx={{
                                [`& .${gaugeClasses.valueText}`]: {
                                  fontSize: 38,
                                  transform: "translate(0px, 0px)",
                                },
                              }}
                              text={({ value }) => `${value} %`}
                            />
                          </div>
                        </div>
                      </FDBox> */
                      <FDBox className="basis-1/4 grow flex flex-row items-center items-stretch">
                        <div className="h-full">
                          <div className="flex justify-center">
                            <h1>Dissolve Oxygen</h1>
                          </div>
                          <div className="h-5/6 w-48 items-center flex justify-center">
                            <div className="py-3 text-4xl m-auto">
                              <div className="font-semibold">
                                {sensor.dissolved_oxygen}
                              </div>
                              <div className="py-3 text-xl text-center">
                                ppm
                              </div>
                            </div>
                          </div>
                        </div>
                      </FDBox>
                    }
                    {
                      <FDBox className="basis-1/4 grow flex flex-row items-center items-stretch">
                        <div className="h-full">
                          <div className="flex justify-center">
                            <h1>Electric Conductivity</h1>
                          </div>
                          <div className="h-5/6 w-48 items-center flex justify-center">
                            <div className="py-3 text-4xl m-auto">
                              <div className="font-semibold">
                                {sensor.electric_conductivity}
                              </div>
                              <div className="py-3 text-xl text-center">
                                ms/cm
                              </div>
                            </div>
                          </div>
                        </div>
                      </FDBox>
                    }
                    {
                      <FDBox className="basis-1/4 grow flex justify-center flex-row items-stretch">
                        <div className="h-full">
                          <div className="flex justify-center">
                            <h1>Temperature</h1>
                          </div>
                          <div className="h-5/6 w-48 items-center flex justify-center">
                            <div className="py-3 text-4xl m-auto">
                              <div className="font-semibold">
                                {sensor.temperature}
                              </div>
                              <div className="py-3 text-xl text-center">°C</div>
                            </div>
                          </div>
                        </div>
                      </FDBox>
                    }
                    {
                      <FDBox className="basis-1/4 grow flex justify-center flex-row items-stretch">
                        <div className="h-full">
                          <div className="flex justify-center">
                            <h1>pH</h1>
                          </div>
                          <div className="h-5/6 w-48 items-center flex justify-center">
                            <div className="py-3 text-4xl m-auto">
                              <div className="font-semibold">{sensor.pH}</div>
                              <div className="py-3 text-xl text-center">
                                {getPHLevel(sensor.pH)}
                              </div>
                            </div>
                          </div>
                        </div>
                      </FDBox>
                    }
                    {
                      <Weather
                        latitude={13.726596724980457}
                        longitude={100.77613121944981}
                        time={currentTime}
                      />
                    }
                  </div>
                  <br />
                </div>
              ))}
            </div>
          ))}
        </div>
        <Chart data={sensorDataTimeSeries} />
      </MainContent>
    </div>
  );
}
export default SensorMonitor;
