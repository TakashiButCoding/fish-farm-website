const config = require("./services/configReader.js");
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger_output.json");
const express = require("express");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const http = require("http");
const https = require("https");
const fs = require("fs");
const bodyParser = require("body-parser");
const passport = require("passport");
const cookieSession = require("cookie-session");
var cors = require("cors");
const e = require("express");
const path = require('path');


const app = express();

// CORS
app.use(cors({credentials: true, origin: 'http://localhost:3002'}));
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3002");
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});
app.use(
  cookieSession({
    name: "google-auth-session",
    keys: ["key1", "key2"],
  })
);

// Google Authentication 
app.use(passport.initialize());
app.use(passport.session());
app.use(
  require("express-session")({
    secret: "fish-farm-website-secret",
    resave: true,
    saveUninitialized: true,
  })
);
passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});
passport.use(
  new GoogleStrategy(
    {
      clientID: global.config.GoogleOpenId.clientId,
      clientSecret: global.config.GoogleOpenId.secret,
      callbackURL: "http://localhost:4000/auth/login",
      passReqToCallback: true,
    },
    function (request, accessToken, refreshToken, profile, done) {
      return done(null, profile);
    }
  )
);

// Other
app.use("/request-type", (req, res, next) => {
  console.log("Request type: ", req.method);
  next();
});
app.use("/public", express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.PORT || 4000;;

http.createServer(app).listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
require("./endpoints")(app);

module.exports = {
};
