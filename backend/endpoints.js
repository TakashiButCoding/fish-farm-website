const databaseController = require("./controller/databaseController.js");
const multer = require("multer");
const passport = require("passport");
require("./app.js");

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const isLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.sendStatus(401);
  }
};

module.exports = function (app) {
  app.get("/api/", (req, res) => {
    res.send("Successful response.");
  });

  app.post("/api/run-query", async (req, res) => {
    databaseController.RunQuery(req, res);
  });
  app.post("/api/getSensorData", async (req, res) => {
    databaseController.getSensorData(req, res);
  });

  app.get("/api/getLatestSensorData", async (req, res) => {
    databaseController.getLatestSensorData(req, res);
  });

  app.post("/api/insertSensorData", async (req, res) => {
    databaseController.InsertSensorData(req, res);
  });

  app.post("/api/upload-image", upload.single("image"), async (req, res) => {
    databaseController.SaveImage(req, res);
  });

  app.post("/api/fetch-images", async (req, res) => {
    databaseController.FetchImages(req, res);
  });

  app.post("/api/fetch-image-by-id", async (req, res) => {
    databaseController.FetchImageById(req, res);
  });

  app.post("/api/upsert-sensor-node", async (req, res) => {
    databaseController.UpsertSensorNode(req, res);
  });

  // ----------fish feed------------
  app.post("/api/upsert-feeder", async (req, res) => {
    databaseController.Upsertfeeder(req, res);
  });
  app.post("/api/upsert-operation-type", async (req, res) => {
    databaseController.UpsertOpType(req, res);
  });
  app.post("/api/insert-feed-log", async (req, res) => {
    databaseController.InsertFeedLog(req, res);
  });
  app.post("/api/update-feed-log", async (req, res) => {
    databaseController.UpdateFeedLog(req, res);
  });
  app.post("/api/insert-transaction", async (req, res) => {
    databaseController.InsertTransaction(req, res);
  });
  app.post("/api/upsert-user", async (req, res) => {
    databaseController.UpsertUser(req, res);
  });
  app.post("/api/reset-money", async (req, res) => {
    databaseController.ResetMoney(req, res);
  });
  app.post("/api/fetch-revenue", async (req, res) => {
    databaseController.FetchRevenue(req, res);
  });
  app.get("/api/coin-allocation", async (req, res) => {
    databaseController.CoinAllocation(req, res);
  });
  app.post("/api/fetch-transaction", async (req, res) => {
    databaseController.FetchTransaction(req, res);
  });
  app.get("/api/total-coin", async (req, res) => {
    databaseController.TotalCoin(req, res);
  });
  app.post("/api/fetch-food-log", async (req, res) => {
    databaseController.FetchFoodLog(req, res);
  });
  app.get("/api/fetch-availiable-food", async (req, res) => {
    databaseController.FetchAvaFood(req, res);
  });
  // --------------------fish feed-------


  app.get("/api/failed", (req, res) => {
    res.send("Failed");
  });

  app.get("/api/success", isLoggedIn, (req, res) => {
    res.send("loging succeess confirm by backend");
  });

  app.get(
    "/api/auth/login",
    passport.authenticate("google", {
      scope: ["email", "profile"],
    })
  );

  app.get(
    "/api/auth/login/callback",
    passport.authenticate("google", {
      failureRedirect: "/api/failed",
    }),
    function (req, res) {
      res.redirect("/api/success");
    }
  );
  app.get("/api/auth/logout", (req, res) => {
    req.session = null;
    req.logout();
    res.redirect("/");
  });
};
