const databaseConnection = require("../services/databaseService.js");
const codeUtil = require("../utilities/codeUtil.js");
async function RunQuery(req, res) {
  try {
    var { database, query } = req.body;
    const results = await databaseConnection.RunQuery(database, query);
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function getSensorData(req, res) {
  try {
    var { timerange_begin, timerange_end } = req.body;
    var {date_start_z, date_end_z} = codeUtil.formatDateRange(timerange_begin, timerange_end, 2);
    const results = await databaseConnection.getSensorData(
      date_start_z,
      date_end_z
    );
    results.forEach((obj, index) => {
      obj["index"] = index;
  });
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function getLatestSensorData(req, res) {
  try {
    const results = await databaseConnection.getLatestSensorData();
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function InsertSensorData(req, res) {
  try {
    var {
      dissolved_oxygen,
      electric_conductivity,
      temperature,
      ph,
      node_id,
      recorded_when,
    } = req.body;
    const results = await databaseConnection.InsertSensorData(
      dissolved_oxygen,
      electric_conductivity,
      ph,
      temperature,
      node_id,
      recorded_when
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function SaveImage(req, res) {
  try {
    // Get the image data from the request
    const imageData = req.file.buffer;
    const originalFilename = req.file.originalname; // Use the original filename of the uploaded file

    // fish metadata
    let metaData = {};
    const image_id = await databaseConnection.SaveImage(
      imageData,
      originalFilename
    );

    if (req.body.json_array) {
      metaData = req.body.json_array;
      const result_metadata = await databaseConnection.SaveImageMetadata(
        image_id,
        metaData
      );
      return res.send({ image_id: image_id, result_metadata: result_metadata });
    }
    return res.send({ image_id: image_id });
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function FetchImages(req, res) {
  try {
    var { pagination_no, fetch_limit, date_start, date_end } = req.body;
    var {date_start_z, date_end_z} = codeUtil.formatDateRange(date_start, date_end, 7)

    const results = await databaseConnection.FetchImages(
      pagination_no,
      fetch_limit,
      date_start_z,
      date_end_z
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function FetchImageById(req, res) {
  try {
    const { image_id } = req.body;
    const results = await databaseConnection.FetchImageById(image_id);
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function UpsertSensorNode(req, res) {
  try {
    var { node_id, description, latitude, longitude } = req.body;
    const results = await databaseConnection.UpsertSensorNode(
      node_id,
      description === null || description.trim() === "" ? null : description,
      latitude,
      longitude
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
// ------------- food feeder ----------------------
async function Upsertfeeder(req, res) {
  try {
    var { feeder_id, description, is_active } = req.body;
    const results = await databaseConnection.Upsertfeeder(
      feeder_id,
      description === null || description.trim() === "" ? null : description,
      is_active
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function UpsertOpType(req, res) {
  try {
    var { operation_type_id, operation_type_name } = req.body;
    const results = await databaseConnection.UpsertOpType(
      operation_type_id,
      operation_type_name
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function InsertFeedLog(req, res) {
  try {
    var {
      feeder_id,
      operation_type_name,
      feed_status_name,
      fed_amount,
      food_tank_percent_before,
      food_tank_percent_after,
      process_started_when,
      process_finished_when,
    } = req.body;
    const results = await databaseConnection.InsertFeedLog(
      feeder_id === null ? 1 : feeder_id,
      operation_type_name,
      feed_status_name,
      fed_amount,
      food_tank_percent_before,
      food_tank_percent_after,
      process_started_when,
      process_finished_when
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function UpdateFeedLog(req, res) {
  try {
    var {
      feeding_log_id,
      feed_status_name,
      fed_amount,
      food_tank_percent_before,
      food_tank_percent_after,
      process_started_when,
      process_finished_when,
    } = req.body;
    const results = await databaseConnection.UpdateFeedLog(
      feeding_log_id,
      feed_status_name,
      fed_amount,
      food_tank_percent_before,
      food_tank_percent_after,
      process_started_when,
      process_finished_when
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function InsertTransaction(req, res) {
  try {
    var {
      feeding_log_id,
      feeder_id,
      payment_type_name,
      user_identifier,
      money_value,
    } = req.body;
    const results = await databaseConnection.InsertTransaction(
      feeding_log_id,
      feeder_id === null ? 1 : feeder_id,
      payment_type_name,
      user_identifier,
      money_value
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function ResetMoney(req, res) {
  try {
    var { feeder_id } = req.body;
    const results = await databaseConnection.ResetMoney(
      feeder_id === null ? 1 : feeder_id
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function UpsertUser(req, res) {
  try {
    var { user_name, user_type, user_identifier } = req.body;
    const results = await databaseConnection.UpsertUser(
      user_name,
      user_type,
      user_identifier
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function FetchRevenue(req, res) {
  try {
    var { date_start, date_end } = req.body;
    
    var {date_start_z, date_end_z} = codeUtil.formatDateRange(date_start, date_end, 7)
    const results = await databaseConnection.FetchRevenue(date_start_z, date_end_z);
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function CoinAllocation(req, res) {
  try {
    const results = await databaseConnection.CoinAllocation();
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function FetchTransaction(req, res) {
  try {
    var { pagination_no, fetch_limit } = req.body;
    const results = await databaseConnection.FetchTransaction(
      pagination_no,
      fetch_limit
    );
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}

async function TotalCoin(req, res) {
  try {
    const results = await databaseConnection.TotalCoin();
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function FetchFoodLog(req, res) {
  try {
    var { date_start, date_end } = req.body;
    
    var {date_start_z, date_end_z} = codeUtil.formatDateRange(date_start, date_end, 7)
    const results = await databaseConnection.FetchFoodLog(date_start_z, date_end_z);
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
async function FetchAvaFood(req, res) {
  try {
    const results = await databaseConnection.FetchAvaFood();
    return res.send(results);
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
}
module.exports = {
  RunQuery,
  getSensorData,
  getLatestSensorData,
  InsertSensorData,
  SaveImage,
  FetchImages,
  FetchImageById,
  UpsertSensorNode,
  Upsertfeeder,
  UpsertOpType,
  InsertTransaction,
  InsertFeedLog,
  UpdateFeedLog,
  InsertTransaction,
  ResetMoney,
  UpsertUser,
  FetchRevenue,
  CoinAllocation,
  FetchTransaction,
  TotalCoin,
  FetchFoodLog,
  FetchAvaFood,
};
