const mysql = require("mysql2/promise");
const codeUtil = require("../utilities/codeUtil.js");

// Database connection configuration
const sensorDB = mysql.createPool({
  host: global.config.mysql.hostname,
  user: global.config.mysql.user,
  password: global.config.mysql.password,
  database: global.config.databases.sensorDB,
  waitForConnections: true,
  connectionLimit: 100,
  queueLimit: 0,
});

const foodDB = mysql.createPool({
  host: global.config.mysql.hostname,
  user: global.config.mysql.user,
  password: global.config.mysql.password,
  database: global.config.databases.foodDB,
  waitForConnections: true,
  connectionLimit: 100,
  queueLimit: 0,
});

const detectionDB = mysql.createPool({
  host: global.config.mysql.hostname,
  user: global.config.mysql.user,
  password: global.config.mysql.password,
  database: global.config.databases.detectionDB,
  waitForConnections: true,
  connectionLimit: 100,
  queueLimit: 0,
});

// Function to execute a query
async function executeQuery(pool, query, params = []) {
  const connection = await pool.getConnection();
  try {
    const [results] = await connection.execute(query, params);
    connection.release();
    return results;
  } catch (error) {
    connection.release();
    throw error;
  }
}
async function RunQuery(database, query) {
  try {
    var db;
    switch (database) {
      case "foodDB":
        db = foodDB;
        break;
      case "sensorDB":
        db = sensorDB;
        break;
      case "detectionDB":
        db = detectionDB;
        break;
      default:
        throw "No Database";
    }
    const [rows, fields] = await executeQuery(db, query);
    return rows;
  } catch (error) {
    console.error("Error RunQuery():", error);
    throw error;
  }
}
async function getSensorData(timerange_begin, timerange_end) {
  try {
    const [rows, fields] = await executeQuery(
      sensorDB,
      "CALL get_sensor_data_v2(?,?)",
      [timerange_begin, timerange_end]
    );
    return rows;
  } catch (error) {
    console.error("Error getSensorData():", error);
    throw error;
  }
}

async function getLatestSensorData() {
  try {
    const [rows, fields] = await executeQuery(
      sensorDB,
      "CALL get_latest_all_sensor_data_v2"
    );
    const roundedRows = rows.map((row) => {
      for (const key in row) {
        if (row.hasOwnProperty(key) && typeof row[key] === "number") {
          row[key] = parseFloat(row[key].toFixed(2));
        }
      }
      if (row.recorded_when) {
        row.recorded_when = codeUtil.simplifyTime(row.recorded_when);
      }
      return row;
    });
    return roundedRows;
  } catch (error) {
    console.error("Error getLatestSensorData():", error);
    throw error;
  }
}

async function InsertSensorData(
  dissolved_oxygen,
  electric_conductivity,
  ph,
  temperature,
  node_id,
  recorded_when
) {
  try {
    dissolved_oxygen = Math.round(dissolved_oxygen * 100) / 100;
    electric_conductivity = Math.round(electric_conductivity * 100) / 100;
    temperature = Math.round(temperature * 100) / 100;
    ph = Math.round(ph * 100) / 100;
    await executeQuery(sensorDB, "CALL insert_sensor_data_v6(?,?,?,?,?,?)", [
      dissolved_oxygen,
      ph,
      electric_conductivity,
      temperature,
      node_id,
      recorded_when,
    ]);
    return true;
  } catch (error) {
    console.error("Error InsertSensorData():", error);
    throw error;
  }
}

async function SaveImage(imageData, originalFilename) {
  let inserted_id = 0;
  try {
    const [rows, fields] = await executeQuery(
      detectionDB,
      "CALL insert_image_v1(?,?)",
      [originalFilename, imageData]
    );
    inserted_id = rows[0].inserted_id;
    return inserted_id;
  } catch (error) {
    console.error("Error SaveImage():", error);
    throw error;
  }
}

async function SaveImageMetadata(inserted_id, metaData) {
  try {
    if (metaData) {
      await executeQuery(detectionDB, "CALL insert_fish_metadata_v4(?,?)", [
        inserted_id,
        metaData,
      ]);
    }
    return true;
  } catch (error) {
    console.error("Error SaveMetaData():", error);
    throw error;
  }
}

async function FetchImages(offset, limit, date_start, date_end) {
  try {
    const [rows, fields] = await executeQuery(
      detectionDB,
      "CALL get_images_pagination_v5(?,?,?,?)",
      [offset, limit, date_start, date_end]
    );
    return rows;
  } catch (error) {
    console.error("Error FetchImages():", error);
    throw error;
  }
}

async function FetchImageById(imageId) {
  try {
    const [rows, fields] = await executeQuery(
      detectionDB,
      "CALL get_image_by_id_v5(?)",
      [imageId]
    );
    const [rows2, fields2] = await executeQuery(
      detectionDB,
      "CALL get_fish_metadata_by_image_id_v3(?)",
      [imageId]
    );
    const result = {
      filename: rows[0].filename,
      data: rows[0].data,
      recorded_when: rows[0].recorded_when,
      metadata: rows2,
    };
    return result;
  } catch (error) {
    console.error("Error FetchImageById():", error);
    throw error;
  }
}

async function UpsertSensorNode(node_id, description, latitude, longitude) {
  try {
    await executeQuery(sensorDB, "CALL upsert_sensor_node_v7(?,?,?,?)", [
      node_id,
      description,
      latitude,
      longitude,
    ]);
    return true;
  } catch (error) {
    console.error("Error UpsertSensorNode():", error);
    throw error;
  }
}

async function Upsertfeeder(feeder_id, description, is_active) {
  try {
    await executeQuery(foodDB, "CALL upsert_feeder_v1(?,?,?)", [
      feeder_id,
      description,
      is_active,
    ]);
    return true;
  } catch (error) {
    console.error("Error Upsertfeeder():", error);
    throw error;
  }
}

async function UpsertOpType(operation_type_id, operation_type_name) {
  try {
    await executeQuery(foodDB, "CALL upsert_operation_type_v1(?,?)", [
      operation_type_id,
      operation_type_name,
    ]);
    return true;
  } catch (error) {
    console.error("Error UpsertOpType():", error);
    throw error;
  }
}

async function InsertFeedLog(
  feeder_id,
  operation_type_name,
  feed_status_name,
  fed_amount,
  food_tank_percent_before,
  food_tank_percent_after,
  process_started_when,
  process_finished_when
) {
  try {
    const [rows, fields] = await executeQuery(
      foodDB,
      "CALL insert_feeding_log_v6(?,?,?,?,?,?,?,?)",
      [
        feeder_id,
        operation_type_name,
        feed_status_name,
        fed_amount,
        food_tank_percent_before,
        food_tank_percent_after,
        process_started_when,
        process_finished_when,
      ]
    );
    return rows;
  } catch (error) {
    console.error("Error InsertFeedLog():", error);
    throw error;
  }
}

async function UpdateFeedLog(
  feeding_log_id,
  feed_status_name,
  fed_amount,
  food_tank_percent_before,
  food_tank_percent_after,
  process_started_when,
  process_finished_when
) {
  try {
    await executeQuery(foodDB, "CALL update_feeding_log_v8(?,?,?,?,?,?,?)", [
      feeding_log_id,
      feed_status_name,
      fed_amount,
      food_tank_percent_before,
      food_tank_percent_after,
      process_started_when,
      process_finished_when,
    ]);
    return true;
  } catch (error) {
    console.error("Error UpdateFeedLog():", error);
    throw error;
  }
}
async function InsertTransaction(
  feeding_log_id,
  feeder_id,
  payment_type_name,
  user_identifier,
  money_value
) {
  try {
    await executeQuery(foodDB, "CALL insert_feeder_transaction_v9(?,?,?,?,?)", [
      feeding_log_id,
      feeder_id,
      payment_type_name,
      user_identifier,
      money_value,
    ]);
    return true;
  } catch (error) {
    console.error("Error InsertTransaction():", error);
    throw error;
  }
}

async function ResetMoney(feeder_id) {
  try {
    await executeQuery(foodDB, "CALL reset_feeder_money_v1(?)", [feeder_id]);
    return true;
  } catch (error) {
    console.error("Error ResetMoney():", error);
    throw error;
  }
}
async function UpsertUser(user_name, user_type, user_identifier) {
  try {
    await executeQuery(foodDB, "CALL upsert_user_v1(?,?,?)", [
      user_name,
      user_type,
      user_identifier,
    ]);
    return true;
  } catch (error) {
    console.error("Error UpsertUser():", error);
    throw error;
  }
}
async function FetchRevenue(date_start, date_end) {
  try {
    const [rows, fields] = await executeQuery(
      foodDB,
      "CALL select_sum_money_value_by_day_v1(?,?)",
      [date_start, date_end]
    );
    return rows;
  } catch (error) {
    console.error("Error FetchRevenue():", error);
    throw error;
  }
}
async function CoinAllocation(date_start, date_end) {
  try {
    const [rows, fields] = await executeQuery(foodDB, "CALL count_coins_v2()");

    return rows;
  } catch (error) {
    console.error("Error FetchRevenue():", error);
    throw error;
  }
}
async function FetchTransaction(pagination_no, fetch_limit) {
  try {
    const [rows, fields] = await executeQuery(
      foodDB,
      "CALL select_transaction_pagination_v1(?,?)",
      [pagination_no, fetch_limit]
    );
    return rows;
  } catch (error) {
    console.error("Error FetchTransaction():", error);
    throw error;
  }
}
async function TotalCoin() {
  try {
    const [rows, fields] = await executeQuery(foodDB, "CALL total_coin_v3()");
    return rows;
  } catch (error) {
    console.error("Error FetchTransaction():", error);
    throw error;
  }
}

async function FetchAvaFood() {
  try {
    const [rows, fields] = await executeQuery(
      foodDB,
      "CALL select_food_availiable_v3()"
    );
    return rows;
  } catch (error) {
    console.error("Error FetchTransaction():", error);
    throw error;
  }
}
async function FetchFoodLog(date_start, date_end) {
  try {
    const [rows, fields] = await executeQuery(
      foodDB,
      "CALL select_food_log_v1(?,?)",
      [date_start, date_end]
    );
    return rows;
  } catch (error) {
    console.error("Error FetchRevenue():", error);
    throw error;
  }
}
module.exports = {
  getSensorData,
  getLatestSensorData,
  InsertSensorData,
  SaveImage,
  SaveImageMetadata,
  FetchImages,
  FetchImageById,
  UpsertSensorNode,
  Upsertfeeder,
  UpsertOpType,
  RunQuery,
  InsertFeedLog,
  UpdateFeedLog,
  InsertTransaction,
  ResetMoney,
  UpsertUser,
  FetchRevenue,
  CoinAllocation,
  FetchTransaction,
  TotalCoin,
  FetchFoodLog,
  FetchAvaFood,
};
