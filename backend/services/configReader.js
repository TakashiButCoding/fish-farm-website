const fs = require('fs');
const path = require('path');

let globalConfig = {};
// Function to read and set global configuration
function loadConfig(configPath) {
  try {
    var secret 
    var db
    if (process.env.NODE_ENV == "qa"){
      secret = fs.readFileSync('./config/secretConfig.json', 'utf8');
      db = fs.readFileSync('./config/databaseConfig.qa.json', 'utf8');
    }
    else if (process.env.NODE_ENV == "production"){
      secret = fs.readFileSync('./config/secretConfig.json', 'utf8');
      db = fs.readFileSync('./config/databaseConfig.prod.json', 'utf8');
    }
    else if (process.env.NODE_ENV == "local"){
      secret = fs.readFileSync('./config/secretConfig.json', 'utf8');
      db = fs.readFileSync('./config/databaseConfig.local.json', 'utf8');
    }
    const dbJson = JSON.parse(db);
    const secretJson = JSON.parse(secret);
    globalConfig = { ...secretJson, ...dbJson };
    global.config = globalConfig
  } catch (error) {
    console.error(`Error loading config from ${configPath}: ${error.message}`);
  }
}

loadConfig();