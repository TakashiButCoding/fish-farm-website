#!/usr/bin/env bash

#cd database/script && ./pull_database_script.sh &
cd backend && npm install && npm run swagger-autogen && NODE_ENV='local' npm run start-dev &
cd frontend && npm install && NODE_ENV='local' npm start