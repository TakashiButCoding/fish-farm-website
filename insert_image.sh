#!/bin/bash

json_array=()
my_array=("catfish" "tilapia")
# Loop to create JSON objects and add them to the array
for i in {1..2}; do
    user="{\"breed\": \"${my_array[i-1]}\", \"count\": $((RANDOM % 50 + 20)), \"fish_density\":$((RANDOM % 50 + 20)), \"estimate\":$((RANDOM % 50 + 20))}"
    json_array+=("$user")
done

json_data=$(IFS=,; echo "[${json_array[*]}]")
echo $json_data
# Make HTTP POST request using curl
curl -X POST --form "image=@C:\Users\Charnkanit Kaewwong\Desktop\image.png" -F "json_array=$json_data" https://smartfish.raikmitl.com/api/upload-image 