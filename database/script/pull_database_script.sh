#!/usr/bin/env bash

# ensure to stop running container
docker  stop    rai-fish-pond-db-test
docker  rm      rai-fish-pond-db-test

# pull latest db
docker  pull    charnkanit/rai-fish-farm-2023:latest
docker  run -d --name rai-fish-pond-db-test -p 3306:3306 charnkanit/rai-fish-farm-2023:latest

# Function to check if MySQL is ready
check_mysql() {
    docker exec rai-fish-pond-db mysqladmin -app_rw -pA2p0p2r3w* ping 2>/dev/null
}

# Wait for MySQL to be ready
until check_mysql; do
    echo "Waiting for MySQL to be ready..."
    sleep 2
done

echo "MySQL is ready! Continue with your tasks."