#!/usr/bin/env bash

# generate backup
docker cp       secret.cnf                      rai-fish-pond-db:/etc/
docker cp       mysqldump.sh                    rai-fish-pond-db:.
docker exec -it rai-fish-pond-db                ./mysqldump.sh
docker cp       rai-fish-pond-db:backup_db.sql  .

# build & push image
docker build -t charnkanit/rai-fish-farm-2023:latest    .
docker push     charnkanit/rai-fish-farm-2023:latest

# clean file
#docker stop     rai-fish-pond-db
#docker rm       rai-fish-pond-db
#docker images -f "dangling=true" -q | xargs docker rmi
rm              backup_db.sql
