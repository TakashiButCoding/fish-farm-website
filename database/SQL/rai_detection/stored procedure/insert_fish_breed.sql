CREATE PROCEDURE insert_fish_breed_v2(
    IN fish_breed_name   VARCHAR(64),
    IN fish_description   VARCHAR(255)
    )
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    INSERT INTO fish_breed (breed_name, description, recorded_when) 
    VALUES (fish_breed_name, fish_description, CURRENT_TIMESTAMP());

END


GRANT EXECUTE ON PROCEDURE insert_fish_breed_v2 TO 'app_rw'@'%';