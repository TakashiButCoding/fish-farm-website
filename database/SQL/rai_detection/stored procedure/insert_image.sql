CREATE PROCEDURE insert_image_v1(
    IN image_name   VARCHAR(255),
    IN image_data   MEDIUMBLOB
    )
BEGIN
    DECLARE inserted_id INT;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    INSERT INTO images (filename, data, recorded_when) VALUES (image_name, image_data, CURRENT_TIMESTAMP());
    SET inserted_id = LAST_INSERT_ID();
    SELECT inserted_id;
END


GRANT EXECUTE ON PROCEDURE insert_image_v1 TO 'app_rw'@'%';
