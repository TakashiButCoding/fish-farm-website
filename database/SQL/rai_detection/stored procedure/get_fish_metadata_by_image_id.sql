CREATE PROCEDURE get_fish_metadata_by_image_id_v3(IN image_id INT) BEGIN
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT b.breed_name,
    map.fish_amount,
    map.fish_density_per_area,
    map.estimate_total_fish
FROM image_fish_mapping as map
    INNER JOIN fish_breed as b ON b.breed_id = map.breed_id
WHERE map.image_id = image_id;
END;
GRANT EXECUTE ON PROCEDURE get_fish_metadata_by_image_id_v3 TO 'app_rw' @'%';