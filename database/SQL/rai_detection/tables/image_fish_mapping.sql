CREATE TABLE image_fish_mapping (
    map_id          INT AUTO_INCREMENT PRIMARY KEY,
    image_id        INT    NOT NULL,
    breed_id        TINYINT UNSIGNED    NOT NULL,
    fish_amount     MEDIUMINT    NULL,
    fish_density_per_area   MEDIUMINT   NULL,
    estimate_total_fish     MEDIUMINT   NULL,
    recorded_when   DATETIME        NOT NULL
);

ALTER TABLE image_fish_mapping
ADD CONSTRAINT fk_image_fish_mapping_image_id
    FOREIGN KEY (image_id) 
    REFERENCES images(id);

ALTER TABLE image_fish_mapping
ADD CONSTRAINT fk_image_fish_mapping_breed_id
    FOREIGN KEY (breed_id) 
    REFERENCES fish_breed(breed_id);
    