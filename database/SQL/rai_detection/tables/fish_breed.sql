CREATE TABLE fish_breed (
    breed_id        TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    breed_name      VARCHAR(64)    NOT NULL,
    description     VARCHAR(255)    NULL,
    recorded_when   DATETIME        NOT NULL
);