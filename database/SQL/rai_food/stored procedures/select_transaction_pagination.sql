CREATE PROCEDURE select_transaction_pagination_v1(
    IN p_pagination INT,
    IN p_limit      TINYINT
)
BEGIN
    DECLARE p_offset INT DEFAULT 0;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SET p_offset = (p_pagination - 1) * p_limit;

    SELECT *
    FROM feeder_transaction
    ORDER BY recorded_when DESC
    LIMIT p_offset, p_limit;
END

GRANT EXECUTE ON PROCEDURE select_transaction_pagination_v1 TO 'app_rw'@'%';