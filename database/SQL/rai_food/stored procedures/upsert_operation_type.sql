CREATE PROCEDURE upsert_operation_type_v1 (
    IN p_operation_type_id TINYINT,
    IN p_operation_type_name VARCHAR(128)
)
BEGIN
    DECLARE existing_id TINYINT;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    -- Check if operation_type_id already exists
    SELECT operation_type_id INTO existing_id
    FROM operation_type
    WHERE operation_type_id = p_operation_type_id;
    
    -- If operation_type_id exists, update the record
    IF existing_id IS NOT NULL THEN
        UPDATE operation_type
        SET operation_type_name = p_operation_type_name,
            updated_when = CURRENT_TIMESTAMP()
        WHERE operation_type_id = p_operation_type_id;
        
    -- If operation_type_id does not exist, insert a new record
    ELSE
        INSERT INTO operation_type (operation_type_name, recorded_when)
        VALUES (p_operation_type_name, CURRENT_TIMESTAMP());
        
    END IF;
    
END

GRANT EXECUTE ON PROCEDURE upsert_operation_type_v1 TO 'app_rw'@'%';