CREATE PROCEDURE select_sum_money_value_by_day_v1(
    IN p_date_start DATE,
    IN p_date_end DATE
)
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SELECT DATE(recorded_when) AS transaction_date, SUM(money_value) AS total_money_value
    FROM feeder_transaction
    WHERE DATE(recorded_when) BETWEEN p_date_start AND p_date_end
    GROUP BY DATE(recorded_when)
    ORDER BY DATE(recorded_when);
END

GRANT EXECUTE ON PROCEDURE select_sum_money_value_by_day_v1 TO 'app_rw'@'%';