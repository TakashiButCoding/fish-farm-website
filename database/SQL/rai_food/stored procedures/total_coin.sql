CREATE PROCEDURE total_coin_v3()
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SELECT SUM(money_value) as 'total_coin'
    FROM feeder_transaction AS ft
    JOIN payment_type AS pt 
    ON ft.payment_type_id = pt.payment_type_id
    WHERE pt.payment_type_name = 'coin';
END;

GRANT EXECUTE ON PROCEDURE total_coin_v3 TO 'app_rw'@'%';