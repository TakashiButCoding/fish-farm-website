CREATE PROCEDURE select_food_availiable_v3()
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SELECT 
        CASE WHEN updated_when IS NULL
            THEN recorded_when
            ELSE updated_when
        END AS log_time,
        CASE WHEN food_tank_percent_after IS NULL
            THEN food_tank_percent_before
            ELSE food_tank_percent_after
        END AS food_percent
    FROM feeding_log
    ORDER BY recorded_when DESC
    LIMIT 1;
END

GRANT EXECUTE ON PROCEDURE select_food_availiable_v3 TO 'app_rw'@'%';