CREATE PROCEDURE count_coins_v2()
BEGIN

    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SELECT money_value, COUNT(money_value) AS coin_count
    FROM feeder_transaction AS ft
    JOIN payment_type AS pt ON ft.payment_type_id = pt.payment_type_id
    WHERE pt.payment_type_name = 'coin'
    GROUP BY money_value;
END;

GRANT EXECUTE ON PROCEDURE count_coins_v2 TO 'app_rw'@'%';