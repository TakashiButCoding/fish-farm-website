CREATE PROCEDURE upsert_user_v1 (
    IN p_user_name VARCHAR(128),
    IN p_user_type VARCHAR(256),
    IN p_user_identifier VARCHAR(256)
)
BEGIN
    DECLARE v_user_type_id TINYINT;
    DECLARE v_user_id TINYINT;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Retrieve user_type_id based on provided user_type
    SET v_user_type_id = (
        SELECT user_type_id 
        FROM user_type
        WHERE user_type = p_user_type
        LIMIT 1
    );

    -- Check if user exists based on user_identifier
    SET v_user_id = (
        SELECT user_id
        FROM user_table
        WHERE user_identifier = p_user_identifier
        LIMIT 1
    );

    IF v_user_id IS NOT NULL THEN
        -- User exists, update the record
        UPDATE user_table
        SET user_name = p_user_name,
            user_type_id = v_user_type_id,
            updated_when = NOW()
        WHERE user_id = v_user_id;
    ELSE
        -- User doesn't exist, insert new record
        INSERT INTO user_table (user_name, user_type_id, user_identifier, recorded_when)
        VALUES (p_user_name, v_user_type_id, p_user_identifier, NOW());
    END IF;
END

GRANT EXECUTE ON PROCEDURE upsert_user_v1 TO 'app_rw'@'%';