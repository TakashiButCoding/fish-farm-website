CREATE PROCEDURE reset_feeder_money_v1(
    IN feeder_id TINYINT
)
BEGIN
    -- Set session transaction isolation level
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    INSERT INTO feeder_money (feeder_id, total_money, recorded_when)
    VALUES (feeder_id, 0, CURRENT_TIMESTAMP());

END;

GRANT EXECUTE ON PROCEDURE reset_feeder_money_v1 TO 'app_rw'@'%';