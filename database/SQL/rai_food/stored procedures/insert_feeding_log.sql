CREATE PROCEDURE insert_feeding_log_v6(
    IN p_feeder_id TINYINT,
    IN p_operation_type_name VARCHAR(128),
    IN p_feed_status_name VARCHAR(52),
    IN p_fed_amount FLOAT,
    IN p_food_tank_percent_before TINYINT,
    IN p_food_tank_percent_after TINYINT,
    IN p_process_started_when DATETIME,
    IN p_process_finished_when DATETIME
)   
BEGIN
    DECLARE operation_type_id_val TINYINT;
    DECLARE feed_status_id_val TINYINT;
    DECLARE inserted_id INT;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Get operation_type_id from operation_type table
	SET operation_type_id_val = (
		SELECT operation_type_id 
		FROM operation_type 
		WHERE operation_type_name = p_operation_type_name
		LIMIT 1
	);
    -- Get feed_status_id from feeding_status table
	SET feed_status_id_val = (
		SELECT status_id 
		FROM feeding_status 
		WHERE status_name = p_feed_status_name
		LIMIT 1
	);
    -- Insert into feeding_log table
    INSERT INTO feeding_log (feeder_id, fed_amount, operation_type_id, food_tank_percent_before, food_tank_percent_after, feed_status_id, recorded_when, process_started_when, process_finished_when)
    VALUES (p_feeder_id, p_fed_amount, operation_type_id_val, p_food_tank_percent_before, p_food_tank_percent_after, feed_status_id_val, CURRENT_TIMESTAMP(), p_process_started_when, p_process_finished_when);
    
    SET inserted_id = LAST_INSERT_ID();
    SELECT inserted_id;
END;

GRANT EXECUTE ON PROCEDURE insert_feeding_log_v6 TO 'app_rw'@'%';