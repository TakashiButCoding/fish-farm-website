CREATE TABLE payment_type (
    payment_type_id     TINYINT AUTO_INCREMENT PRIMARY KEY,
    payment_type_name   VARCHAR(128)    NOT NULL,
    recorded_when       DATETIME        NOT NULL,
    updated_when        DATETIME        NULL
);
