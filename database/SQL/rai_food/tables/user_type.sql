CREATE TABLE user_type (
    user_type_id        TINYINT AUTO_INCREMENT PRIMARY KEY,
    user_type           VARCHAR(256)    NULL,
    recorded_when       DATETIME        NOT NULL,
    updated_when        DATETIME        NULL
);
