CREATE TABLE operation_type (
    operation_type_id     TINYINT AUTO_INCREMENT PRIMARY KEY,
    operation_type_name   VARCHAR(128)    NOT NULL,
    recorded_when       DATETIME        NOT NULL,
    updated_when        DATETIME        NULL
);
