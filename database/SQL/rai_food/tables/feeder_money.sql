CREATE TABLE feeder_money (
    log_id             INT AUTO_INCREMENT PRIMARY KEY,
    feeder_id           TINYINT         NOT NULL,
    total_money         INT             DEFAULT(0),
    recorded_when       DATETIME        NOT NULL
);

ALTER TABLE feeder_money
ADD CONSTRAINT fk_feeder_money_feeder_id
    FOREIGN KEY (feeder_id) 
    REFERENCES feeder(feeder_id);