CREATE USER 'app_rw'@'%' IDENTIFIED BY '[APP_RW_PASSWORD]';

ALTER USER 'app_rw'@'%' IDENTIFIED BY '[APP_RW_PASSWORD]';

GRANT SELECT ON rai_sensor.* TO 'app_rw'@'%';
GRANT INSERT ON rai_sensor.* TO 'app_rw'@'%';
GRANT DELETE ON rai_sensor.* TO 'app_rw'@'%';
GRANT UPDATE ON rai_sensor.* TO 'app_rw'@'%';

GRANT SELECT ON qa_rai_sensor.* TO 'app_rw'@'%';
GRANT INSERT ON qa_rai_sensor.* TO 'app_rw'@'%';
GRANT DELETE ON qa_rai_sensor.* TO 'app_rw'@'%';
GRANT UPDATE ON qa_rai_sensor.* TO 'app_rw'@'%';

GRANT SELECT ON rai_detection.* TO 'app_rw'@'%';
GRANT INSERT ON rai_detection.* TO 'app_rw'@'%';
GRANT DELETE ON rai_detection.* TO 'app_rw'@'%';
GRANT UPDATE ON rai_detection.* TO 'app_rw'@'%';

GRANT SELECT ON qa_rai_detection.* TO 'app_rw'@'%';
GRANT INSERT ON qa_rai_detection.* TO 'app_rw'@'%';
GRANT DELETE ON qa_rai_detection.* TO 'app_rw'@'%';
GRANT UPDATE ON qa_rai_detection.* TO 'app_rw'@'%';

GRANT SELECT ON rai_food.* TO 'app_rw'@'%';
GRANT INSERT ON rai_food.* TO 'app_rw'@'%';
GRANT DELETE ON rai_food.* TO 'app_rw'@'%';
GRANT UPDATE ON rai_food.* TO 'app_rw'@'%';

GRANT SELECT ON qa_rai_food.* TO 'app_rw'@'%';
GRANT INSERT ON qa_rai_food.* TO 'app_rw'@'%';
GRANT DELETE ON qa_rai_food.* TO 'app_rw'@'%';
GRANT UPDATE ON qa_rai_food.* TO 'app_rw'@'%';

FLUSH PRIVILEGES;