CREATE TABLE sensor_node (
    sensor_node_id          TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    description_text        VARCHAR(512)       NULL,
    recorded_when           DATETIME    NOT NULL,
    updated_when            DATETIME    NULL,
    latitude                FLOAT       NULL,
    longitude               FLOAT       NULL
);
