CREATE TABLE sensor_data (
    sensor_data_id          INT AUTO_INCREMENT PRIMARY KEY,
    sensor_node_id          TINYINT UNSIGNED    NOT NULL,
    dissolved_oxygen_ppm    FLOAT       NULL,
    electric_conductivity_ms_cm FLOAT       NULL,
    ph                      FLOAT       NULL,
    temperature_c           FLOAT       NULL,
    recorded_when           DATETIME    NOT NULL
);

ALTER TABLE sensor_data
ADD CONSTRAINT fk_sensor_data_sensor_node_id
    FOREIGN KEY (sensor_node_id) 
    REFERENCES sensor_node(sensor_node_id);