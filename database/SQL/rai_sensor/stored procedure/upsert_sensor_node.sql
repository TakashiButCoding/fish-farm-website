CREATE PROCEDURE upsert_sensor_node_v7(
    IN node_id          TINYINT,
    IN node_description VARCHAR(512),
    IN node_latitude    FLOAT,
    IN node_longitude   FLOAT
)
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    INSERT INTO sensor_node (sensor_node_id, description_text, latitude, longitude, recorded_when, updated_when)
    VALUES (node_id, node_description, node_latitude, node_longitude, CURRENT_TIMESTAMP(), NULL)
    ON DUPLICATE KEY UPDATE 
        description_text = IF(node_description = description_text OR node_description IS NULL, description_text, node_description),
        latitude = node_latitude,
        longitude = node_longitude,
        updated_when = CURRENT_TIMESTAMP();
END


GRANT EXECUTE ON PROCEDURE upsert_sensor_node_v7 TO 'app_rw'@'%';