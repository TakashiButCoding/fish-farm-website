CREATE PROCEDURE insert_sensor_data_v6(
    IN dissolved_oxygen FLOAT,
    IN ph               FLOAT,
    IN electric_conductivity    FLOAT,
    IN temperature      FLOAT,
    IN node_id          TINYINT,
    IN recorded_when    DATETIME
)
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    INSERT INTO sensor_data (
        sensor_node_id,
        dissolved_oxygen_ppm,
        electric_conductivity_ms_cm,
        ph,
        temperature_c,
        recorded_when
    )
    VALUES (
        node_id,
        dissolved_oxygen,
        electric_conductivity,
        ph,
        temperature,
        recorded_when
    );
END

GRANT EXECUTE ON PROCEDURE insert_sensor_data_v6 TO 'app_rw'@'%';