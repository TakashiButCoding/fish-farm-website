CREATE PROCEDURE get_sensor_data_v2(
    IN timerange_begin    DATETIME,
    IN timerange_end    DATETIME
)
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SELECT 
        dissolved_oxygen_ppm as 'dissolved_oxygen',
        electric_conductivity_ms_cm as 'electric_conductivity',
        ph as 'pH',
        temperature_c as 'temperature',
        sensor_node_id as 'sensor_node_id',
        recorded_when as 'recorded_when'
    FROM sensor_data
    WHERE recorded_when BETWEEN timerange_begin and timerange_end;
END


GRANT EXECUTE ON PROCEDURE get_sensor_data_v2 TO 'app_rw'@'%';