#!/bin/bash
export TZ="Asia/Bangkok"
# Generate random data for dissolved_oxygen, turbidity, ph, and node_id
dissolved_oxygen1=$(awk -v min=0 -v max=25 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')
turbidity1=$(awk -v min=0 -v max=100 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')
ph1=$(awk -v min=0 -v max=14 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')
temp1=$(awk -v min=20 -v max=50 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')

dissolved_oxygen2=$(awk -v min=0 -v max=25 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')
turbidity2=$(awk -v min=0 -v max=100 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')
ph2=$(awk -v min=0 -v max=14 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')
temp2=$(awk -v min=20 -v max=50 -v seed=$RANDOM 'BEGIN{srand(seed); print min+rand()*(max-min)}')

# Construct JSON request body
json_data1="{\"dissolved_oxygen\": $dissolved_oxygen1, \"electric_conductivity\": $turbidity1, \"temperature\": $temp1, \"ph\": $ph1, \"node_id\": 1, \"recorded_when\": \"$(date +"%Y-%m-%dT%H:%M:%S")\"}"
json_data2="{\"dissolved_oxygen\": $dissolved_oxygen2, \"electric_conductivity\": $turbidity2, \"temperature\": $temp2, \"ph\": $ph2, \"node_id\": 2, \"recorded_when\": \"$(date +"%Y-%m-%dT%H:%M:%S")\"}"
echo $json_data1
# Make HTTP POST request using curl
curl -X POST -H "Content-Type: application/json" -d "$json_data1" http://localhost:4000/api/insertSensorData
#curl -X POST -H "Content-Type: application/json" -d "$json_data2" http://localhost:4000/api/insertSensorData
